<p align="center"><a href="https://laravel.com" target="_blank">
<img src="https://thumbs.dreamstime.com/b/football-players-yellow-stroke-background-tournament-league-banner-poster-design-118943025.jpg" width="400"></a></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework">
<img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework">
<img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Web site
**[https://insider.neostep.az/](https://insider.neostep.az)**

## Technologies

- **Laravel 9.02**
- **Jquery**
- **Ajax**
- **Bootstrap 4**

## About Project

This project is designed to create a league based on the teams included in the system, to compile a match schedule, to
monitor weekly results and forecasts.

## About weekly match

In the league compiled from the selected teams, a match schedule is prepared based on the Round-robin-tournament algorithm.

- **[Round-robin-tournament](https://en.wikipedia.org/wiki/Round-robin_tournament)**

Weekly match predictions are calculated based on the average point of the teams participating in the league for the previous weeks.

Note: Match predictions start to be calculated after playing 60% of the total number of weeks (some teams may score 0 points in the first weeks).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
