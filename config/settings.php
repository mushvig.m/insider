<?php

return [
    'teams' => [
        'Man City',
        'Liverpool',
        'Chelsea',
        'Man Utd',
        'West Ham',
        'Arsenal',
        'Wolverhampton',
        'Tottenham',
        'Brighton',
        'Southampton',
        'Leicester',
        'Aston Villa',
        'Crystal Palace',
        'Brentford',
        'Leeds United',
        'Everton',
        'Newcastle',
        'Watford',
        'Burnley',
        'Norwich',
    ],
];
