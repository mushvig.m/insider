<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->integer('week')->default(1);
            $table->foreignId('team_one_id')->constrained('teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_two_id')->constrained('teams')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('team_one_goals')->default(0);
            $table->integer('team_two_goals')->default(0);
            $table->tinyInteger('win')->default(0)->comment('0 - pending, 1 - first, 2 - second, 3 - drawn');
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
};
