<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (count(config('settings.teams'))) {
            foreach (config('settings.teams') as $key => $team) {
                DB::table('teams')->insert([
                    'name' => $team,
                    'order' => ++$key,
                ]);
            }
        }
    }
}
