$(document).ready(function () {
    getResults($('#next-week'))
});


$('#next-week').on('click', function (e) {
    e.preventDefault();

    getResults($(this));
});

function getResults(object) {
    //ELEMENTS
    let results = $('.results');
    let buttons = $('.buttons');
    let week = object.attr('data-week');
    //END ELEMENTS

    //Request to API
    $.ajax({
        headers: headers,
        url: '/api/results',
        data: JSON.stringify({
            'week': week
        }),
        method: 'post',
        dataType: "json",
        beforeSend: function () {
            results.css("opacity", "0.4");
        },
        success: function (response) {
            if (response.success === true) {
                results.css("opacity", "1");

                //matches block empty
                results.empty();

                //matches block create
                results.append(appendResponseStatistics(response.data.results.teamStatistics));

                results.append(appendResponseMatchResults(response.data.results.matchResults, week));

                if (response.data.results.matchPredictions.length) {
                    results.append(appendResponseMatchPredictions(response.data.results.matchPredictions, week));
                }

                if (response.data.results.lastWeek === parseInt(week)) {
                    buttons.remove();

                    getSwalTeamName(response.data.results.teamStatistics[0].team.name);

                }

                //increment or decrement week
                object.attr('data-week', ++week);
            }
        },
        error: function (response) {
            Swal.fire({
                icon: 'error',
                title: response.responseJSON.message,
            })

            Swal.fire({
                icon: 'error',
                title: response.responseJSON.message,
                showDenyButton: false,
                showCancelButton: false,
                confirmButtonText: 'Try again',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location.href = '/';
                }
            })
        }
    });
    //END Request to API
}


//PLAY ALL

$('#play-all').on('click', function (e) {
    e.preventDefault();

    getPlayAllResults($(this));
});

function getPlayAllResults(object) {
    //ELEMENTS
    let results = $('.results');
    let buttons = $('.buttons');
    let week = object.attr('data-week');
    //END ELEMENTS

    //Request to API
    $.ajax({
        headers: headers,
        url: '/api/results-for-play-all',
        method: 'post',
        dataType: "json",
        beforeSend: function () {
            results.css("opacity", "0.4");
        },
        success: function (response) {
            if (response.success === true) {

                results.css("opacity", "1");

                //matches block empty
                results.empty();

                //matches block create
                for (let i = 0; i < response.data.results.matchResults.length; i++) {
                    let weekNumber = i + 1;
                    if (response.data.results.teamStatistics[i].length) {
                        results.append(appendResponseStatistics(response.data.results.teamStatistics[i]));
                    }
                    if (response.data.results.matchResults[i].length) {
                        results.append(appendResponseMatchResults(response.data.results.matchResults[i], weekNumber));
                    }
                    if (response.data.results.matchPredictions[i].length) {
                        results.append(appendResponseMatchPredictions(response.data.results.matchPredictions[i], weekNumber));
                    }
                }

                buttons.remove();

                getSwalTeamName(response.data.results.teamStatistics[0][0].team.name);
            }
        },
        error: function (response) {
            Swal.fire({
                icon: 'error',
                title: response.responseJSON.message,
            })
        }
    });
    //END Request to API
}

//END PLAY ALL

//Append match data
function appendResponseStatistics(teamStatistics) {
    let html = `<div class="col-lg-6">
                        <p class="header">League Table</p>
                        <table class="table-striped table-hover result-point">
                            <thead class="point-table-head">
                            <tr class="text-center">
                                <th>TEAMS</th>
                                <th>PTS</th>
                                <th>P</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>GD</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">`;
    $.each(teamStatistics, function (key, teamStatistic) {
        html += `<tr>
                                <td>${teamStatistic.team.name}</td>
                                <td>${teamStatistic.points}</td>
                                <td>${teamStatistic.played}</td>
                                <td>${teamStatistic.won}</td>
                                <td>${teamStatistic.drawn}</td>
                                <td>${teamStatistic.lost}</td>
                                <td>${teamStatistic.gd}</td>
                            </tr>`;
    });
    html += `</tbody>
                        </table>
                    </div>`;


    return html;
}

function appendResponseMatchResults(matchResults, week) {
    let html = `<div class="col-lg-3">
                        <p class="header">Match results</p>
                        <div class="championship">
                            <p>${week}th week match result</p>`;
    $.each(matchResults, function (key, match) {
        html += `<div class="match-results">
                                <span>${match.team_one}</span>
                                <span>${match.team_one_goals} - ${match.team_two_goals}</span>
                                <span>${match.team_two}</span>
                            </div>`;
    });
    html += `</div>
                    </div>`;

    return html;
}

function appendResponseMatchPredictions(matchPredictions, week) {
    let html = `<div class="col-lg-3">
                        <p class="header">Championship</p>
                        <div class="championship">
                            <p>${week}th week predictions of championship</p>`;
    $.each(matchPredictions, function (key, matchPrediction) {
        html += `<div>
                                <span>${matchPrediction.team}</span>
                                <span>${matchPrediction.team_percent} %</span>
                            </div>`;
    });
    html += `</div>
                    </div>`;
    return html;
}


function getSwalTeamName(teamName) {
    Swal.fire({
        title: 'The team that is the champion: ' + teamName,
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Create a tournament again',
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            window.location.href = '/';
        }
    })
}

//END Append match data
