const token = document.head.querySelector('meta[name="csrf-token"]').getAttribute('content');
const headers = {
    "X-CSRF-TOKEN": token
};

$.ajaxSetup({
    contentType: "application/json; charset=utf-8"
});
