//create tournament schedule
$(document).ready(function () {
    createTournamentSchedule($('#create-tournament-schedule'), true)
});

$('#create-tournament-schedule').on('click', function (e) {
    e.preventDefault();

    createTournamentSchedule($(this));
});

function createTournamentSchedule(object, load = false) {
    //ELEMENTS
    let matches = $('.matches');
    let sectionBlock = $('.section-block');
    let teamCount = $('input[name="teamCount"]');
    let start = ` <div class="row start-button-block">
                <div class="col-lg-12 mt-3 d-flex justify-content-center">
                    <a href="/weekly-results" class="btn btn-primary">Start for the first week</a>
                </div>
            </div>`;
    //END ELEMENTS

    //input validate check
    if (!validateTournamentSchedule(teamCount, load)) return false;
    //END input validate check

    //Request to API
    $.ajax({
        headers: headers,
        url: '/api/create-tournament-schedule',
        data: JSON.stringify({
            'teamCount': teamCount.val(),
            'load': load,
        }),
        method: 'post',
        dataType: "json",
        beforeSend: function () {
            matches.css("opacity", "0.4");
        },
        success: function (response) {
            matches.css("opacity", "1");

            object.text('Regenerate');

            //start for this week remove
            sectionBlock.find('.start-button-block').remove();

            //start for this week create
            sectionBlock.append(start);

            //matches block empty
            matches.empty();

            //matches block create
            matches.append(appendResponse(response));
        }
    });
    //END Request to API
}

//Validate check input
function validateTournamentSchedule(teamCount, load) {
    teamCount.next('.field-error').remove();
    teamCount.css("border-color", "#ced4da");

    if (!load) {
        if (!teamCount.val()) {
            teamCount.css("border-color", "#FF0000");
            teamCount.after('<p class="field-error">This field must be filled</p>');
            return false;
        }

        if (teamCount.val() && (teamCount.val() < 4 || teamCount.val() > 20)) {
            teamCount.css("border-color", "#FF0000");
            teamCount.after('<p class="field-error">The count of teams should be between 4 and 20</p>');
            return false;
        }
    }

    return true;
}

//END Validate check input

//Append match data
function appendResponse(response) {
    let html = '';

    $.each(response.data, function (key, weekMatch) {
        html += ` <div class="col-lg-3 mt-3">
                    <div class="championship">
                        <p>${key}th week match result</p>`;
        $.each(weekMatch, function (key, match) {
            html += ` <div>
                            <span>${match.team_one}</span>
                            <span> - </span>
                            <span>${match.team_two}</span>
                        </div>`;
        });
        html += ` </div>
                </div>`;
    });

    return html;
}

//END Append match data

//END create tournament schedule



