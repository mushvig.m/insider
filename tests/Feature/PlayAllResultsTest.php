<?php

namespace Tests\Feature;

use App\Http\Requests\WeeklyResultsRequest;
use Tests\TestCase;

class PlayAllResultsTest extends TestCase
{
    /**
     * Results for play all matches.
     *
     * @return void
     */
    public function test_get_play_all_results()
    {
        $response = $this->withHeaders([
            "X-CSRF-TOKEN" => csrf_token(),
        ])->post('/api/results-for-play-all', []);

        $this->withExceptionHandling();

        $response->assertStatus(200);
    }
}
