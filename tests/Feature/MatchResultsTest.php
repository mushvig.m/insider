<?php

namespace Tests\Feature;

use App\Http\Requests\WeeklyResultsRequest;
use Tests\TestCase;

class MatchResultsTest extends TestCase
{
    /**
     * Check validation for match results.
     *
     * @return void
     */
    public function test_request_correct_validate_match_results(): void
    {
        $request = new WeeklyResultsRequest();

        //but must integer
        $this->assertEquals([
            'week' => ['required', 'integer'],
        ], $request->rules());
    }

    /**
     * Send request without week variable for results api
     *
     * @return void
     */
    public function test_request_without_week(): void
    {
        $response = $this->withHeaders([
            "X-CSRF-TOKEN" => csrf_token(),
        ])->post('/api/results', []);

        $response->assertStatus(422);
    }

    /**
     * Weekly results.
     *
     * @return void
     */
    public function test_get_current_week_results(): void
    {
        $data = [
            'week' => 1,
        ];

        $response = $this->withHeaders([
            "X-CSRF-TOKEN" => csrf_token(),
        ])->post('/api/results', $data);

        $this->withExceptionHandling();

        $response->assertStatus(200);
    }
}
