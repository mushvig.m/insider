<?php

namespace Tests\Feature;

use App\Http\Requests\CreateTournamentScheduleRequest;
use Tests\TestCase;

class CreateTournamentScheduleTest extends TestCase
{
    /**
     * Check validation for creat tournament.
     *
     * @return void
     */
    public function test_request_correct_validate_tournament_schedule(): void
    {
        $request = new CreateTournamentScheduleRequest();

        //but must integer
        $this->assertEquals([
            'teamCount' => ['required_if:load,false', 'integer', 'min:4', 'max:20'],
        ], $request->rules());
    }

    /**
     * Send request without teamCount variable for results api
     *
     * @return void
     */
    public function test_request_without_teamCount(): void
    {
        $response = $this->withHeaders([
            "X-CSRF-TOKEN" => csrf_token(),
        ])->post('/api/results', []);

        $response->assertStatus(422);
    }

    /**
     * Create tournament schedule.
     *
     * @return void
     */
    public function test_create_tournament_schedule(): void
    {
        $data = [
            'teamCount' => 4,
            'load' => false
        ];

        $response = $this->withHeaders([
            "X-CSRF-TOKEN" => csrf_token(),
        ])->post('/api/create-tournament-schedule', $data);

        $this->withExceptionHandling();

        $response->assertStatus(200);
    }
}
