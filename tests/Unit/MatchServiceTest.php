<?php

namespace Tests\Unit;

use App\Repositories\Interfaces\IMatchRepository;
use App\Services\MatchService;
use App\Services\TeamService;
use App\Services\TeamStatisticService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MatchServiceTest extends TestCase
{
    /**
     * The match repository implementation.
     *
     * @var IMatchRepository
     */
    protected IMatchRepository $matchRepositoryMock;

    /**
     * The team service implementation.
     *
     * @var TeamService
     */
    protected TeamService $teamServiceMock;

    /**
     * The team statistic service implementation.
     *
     * @var TeamStatisticService
     */
    protected TeamStatisticService $teamStatisticServiceMock;

    /**
     * The match service implementation.
     *
     * @var MatchService
     */
    protected MatchService $matchService;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->matchRepositoryMock = m::mock(IMatchRepository::class);

        $this->teamServiceMock = m::mock(TeamService::class);

        $this->teamStatisticServiceMock = m::mock(TeamStatisticService::class);

        $this->teamServiceMock->shouldReceive('getCount')->once()->andReturn(4);

        $this->matchService = new MatchService($this->matchRepositoryMock, $this->teamServiceMock, $this->teamStatisticServiceMock);
    }

    /**
     * Generate fixture - Robin round algorithm.
     *
     * @return void
     */
    public function test_generate_fixture()
    {
        $results = $this->matchService->generateFixture([]);

        $this->assertEquals([], $results);

        $results = $this->matchService->generateFixture([
            0 => ['id' => 1, 'name' => "Man City"],
            1 => ['id' => 2, 'name' => "Liverpool"],
            2 => ['id' => 3, 'name' => "Chelsea"],
            3 => ['id' => 4, 'name' => "Man Utd"]
        ]);

        $this->assertIsArray($results);
    }
}
