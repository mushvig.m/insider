<?php

namespace Tests\Unit;

use App\Repositories\Interfaces\ITeamRepository;
use App\Services\TeamService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TeamServiceTest extends TestCase
{
    /**
     * The team repository implementation.
     *
     * @var ITeamRepository
     */
    protected ITeamRepository $teamRepositoryMock;

    /**
     * The team service implementation.
     *
     * @var TeamService
     */
    protected TeamService $teamService;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->teamRepositoryMock = m::mock(ITeamRepository::class);

        $this->teamService = new TeamService($this->teamRepositoryMock);
    }

    /**
     * Create team.
     *
     * @return void
     */
    public function test_create_team(): void
    {
        $this->teamRepositoryMock->shouldReceive('updateAllActive')->once();
        $this->teamRepositoryMock->shouldReceive('getRandomTeam')->once()->times(4);
        $this->teamRepositoryMock->shouldReceive('setId')->once()->times(4);
        $this->teamRepositoryMock->shouldReceive('createTeam')->times(4);

        $this->teamService->createTeam(4);

        $this->assertTrue(true);
    }

    /**
     * Team count.
     *
     * @return void
     */
    public function test_team_count(): void
    {
        $this->teamRepositoryMock->shouldReceive('getCount')->once()->andReturn(4);

        $teamCount = $this->teamService->getCount();

        $this->assertEquals(4, $teamCount);
    }
}
