<?php

namespace Tests\Unit;

use App\Repositories\Interfaces\ITeamRepository;
use App\Repositories\Interfaces\ITeamStatisticRepository;
use App\Services\TeamService;
use App\Services\TeamStatisticService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TeamStatisticServiceTest extends TestCase
{
    /**
     * The team repository implementation.
     *
     * @var ITeamStatisticRepository
     */
    protected ITeamStatisticRepository $teamStatisticRepositoryMock;

    /**
     * The team statistic service implementation.
     *
     * @var TeamStatisticService
     */
    protected TeamStatisticService $teamStatisticService;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->teamStatisticRepositoryMock = m::mock(ITeamStatisticRepository::class);

        $this->teamStatisticService = new TeamStatisticService($this->teamStatisticRepositoryMock);
    }

    /**
     * Team last match statistics.
     *
     * @return void
     */
    public function test_team_last_match_statistics(): void
    {
        $teamRepository = m::mock(ITeamRepository::class);

        $teamService = new TeamService($teamRepository);

        $teamRepository->shouldReceive('getAllActive')->once()->andReturn([0 => ['id' => 1, 'name' => 'Man City'], 1 => ['id' => 2, 'name' => 'Liverpool']]);

        $team = $teamService->getAllActive()[0];

        $this->teamStatisticRepositoryMock->shouldReceive('lastMatchStatistic')->with($team['id'])->once();

        $data = $this->teamStatisticService->lastMatchStatistic($team['id']);

        $this->assertEquals(null, $data);
    }

    /**
     * Delete all team statistics.
     *
     * @return void
     */
    public function test_delete_all_team_statistics(): void
    {
        $this->teamStatisticRepositoryMock->shouldReceive('deleteAllStatistics')->once();

        $this->teamStatisticService->deleteAllStatistics();

        $this->assertTrue(true);
    }

    /**
     * Save statistics.
     *
     * @return void
     */
    public function test_save_statistics(): void
    {
        $attributes = [
            'week' => 1,
            'team_id' => 1,
            'points' => 224,
            'played' => 1,
            'won' => 1,
            'drawn' => 0,
            'lost' => 0,
            'gd' => 4,
        ];

        $this->teamStatisticRepositoryMock->shouldReceive('saveStatistics')->with($attributes)->once();

        $this->teamStatisticService->saveStatistics($attributes);

        $this->assertTrue(true);
    }


}
