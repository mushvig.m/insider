<?php

namespace App\Services;

use App\Models\TeamStatistic;
use App\Repositories\Interfaces\ITeamStatisticRepository;
use Illuminate\Support\Collection;

class TeamStatisticService
{
    /**
     * The team interface statistic repository implementation.
     *
     * @var ITeamStatisticRepository
     */
    protected ITeamStatisticRepository $teamStatisticRepository;

    /**
     * Create a new team interface statistic repository instance.
     *
     * @param ITeamStatisticRepository $teamStatisticRepository
     * @return void
     */
    public function __construct(ITeamStatisticRepository $teamStatisticRepository)
    {
        $this->teamStatisticRepository = $teamStatisticRepository;
    }

    /**
     * Delete statistics.
     *
     * @return void
     */
    public function deleteAllStatistics(): void
    {
        $this->teamStatisticRepository->deleteAllStatistics();
    }

    /**
     * Last match statistic.
     *
     * @param int $teamId
     * @return TeamStatistic|null
     */
    public function lastMatchStatistic(int $teamId): TeamStatistic|null
    {
        return $this->teamStatisticRepository->lastMatchStatistic($teamId);
    }

    /**
     * Save statistics.
     *
     * @param array $attributes
     * @return void
     */
    public function saveStatistics(array $attributes): void
    {
        $this->teamStatisticRepository->saveStatistics($attributes);
    }


    /**
     * Team statistics.
     *
     * @param int $week
     * @return Collection
     */
    public function teamStatistics(int $week): Collection
    {
        $this->teamStatisticRepository->setWeek($week);

        return $this->teamStatisticRepository->teamStatistics();
    }

    /**
     * First stage point.
     *
     * @param int $week
     * @return int
     */
    public function firstStagePoint(int $week): int
    {
        $this->teamStatisticRepository->setWeek($week);

        return $this->teamStatisticRepository->firstStagePoint();
    }


    /**
     * Possible champion points sum.
     *
     * @param int $week
     * @param int $firstStageTeamPoints
     * @param int $potentialPoints
     * @return int
     */
    public function possibleChampionsPointsSum(int $week, int $firstStageTeamPoints, int $potentialPoints): int
    {
        $this->teamStatisticRepository->setWeek($week);

        return $this->teamStatisticRepository->possibleChampionsPointsSum($firstStageTeamPoints, $potentialPoints);
    }

    /**
     * Check any zero point.
     *
     * @param int $week
     * @return bool
     */
    public function checkAnyZeroPoint(int $week): bool
    {
        $this->teamStatisticRepository->setWeek($week);

        return $this->teamStatisticRepository->checkAnyZeroPoint();
    }

}
