<?php

namespace App\Services;


use App\Repositories\Interfaces\ITeamRepository;

class TeamService
{
    /**
     * The team interface repository implementation.
     *
     * @var ITeamRepository
     */
    protected ITeamRepository $teamRepository;

    /**
     * Create a new team interface repository instance.
     *
     * @param ITeamRepository $teamRepository
     * @return void
     */
    public function __construct(ITeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * Create team
     *
     * @param int $teamCount
     * @return void
     */
    public function createTeam(int $teamCount): void
    {
        $this->updateAllActive();

        for ($i = 1; $i <= $teamCount; $i++) {
            $this->teamRepository->setId($this->getRandomTeam());
            $this->teamRepository->createTeam();
        }
    }

    /**
     * Update active Teams.
     *
     * @return void
     */
    private function updateAllActive(): void
    {
        $this->teamRepository->updateAllActive();
    }

    /**
     * Get random Team.
     *
     * @return int
     */
    private function getRandomTeam(): int
    {
        return $this->teamRepository->getRandomTeam();
    }

    /**
     * Get all active Team.
     *
     * @return array
     */
    public function getAllActive(): array
    {
        return $this->teamRepository->getAllActive();
    }

    /**
     * Get team count.
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->teamRepository->getCount();
    }

}
