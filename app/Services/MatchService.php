<?php

namespace App\Services;


use App\Exceptions\AnyMatchNotFoundException;
use App\Exceptions\ThisWeekMatchNotFoundException;
use App\Exceptions\WeekMatchNotFoundException;
use App\Http\Requests\CreateTournamentScheduleRequest;
use App\Http\Resources\MatchResource;
use App\Http\Resources\PredictionResource;
use App\Models\Matches;
use App\Repositories\Interfaces\IMatchRepository;
use Exception;
use Illuminate\Support\Collection;

class MatchService
{
    /**
     * The id implementation.
     *
     * @var int
     */
    protected int $teamCount;

    /**
     * The week implementation.
     *
     * @var int
     */
    protected int $weekCount;

    /**
     * The match interface repository implementation.
     *
     * @var IMatchRepository
     */
    protected IMatchRepository $matchRepository;

    /**
     * The team service implementation.
     *
     * @var TeamService
     */
    protected TeamService $teamService;

    /**
     * The team statistic service implementation.
     *
     * @var TeamStatisticService
     */
    protected TeamStatisticService $teamStatisticService;

    /**
     * Create a new match interface repository instance.
     *
     * @param IMatchRepository $matchRepository
     * @return void
     */
    public function __construct(IMatchRepository $matchRepository, TeamService $teamService, TeamStatisticService $teamStatisticService)
    {
        $this->matchRepository = $matchRepository;
        $this->teamService = $teamService;
        $this->teamStatisticService = $teamStatisticService;
        $this->teamCount = $this->teamService->getCount();
        $this->weekCount = getWeekCount($this->teamCount);
    }

    /**
     * Create match.
     *
     * @param CreateTournamentScheduleRequest $request
     * @return Collection
     */
    public function createMatch(CreateTournamentScheduleRequest $request): Collection
    {
        $teamCount = $request->validated()['teamCount'];

        if (!$request->load) {

            $this->matchRepository->deleteAllMatch();

            $this->teamStatisticService->deleteAllStatistics();

            $this->teamService->createTeam($teamCount);

            $this->saveMatch($this->generateFixture($this->teamService->getAllActive()), $teamCount);
        }

        return $this->getAllMatches();
    }

    /**
     * Robin round algorithm.
     *
     * @param array $teams
     * @return array
     */
    public function generateFixture(array $teams): array
    {
        $weekMatches = [];

        if (count($teams) % 2 != 0) {
            array_push($teams, ['id' => 0, 'name' => ""]);
        }

        $teamCount = count($teams);

        $map = array_keys($teams);

        $teamCountMid = $teamCount / 2;

        for ($i = 0; $i < $teamCount - 1; $i++) {
            $l1 = array_slice($map, 0, $teamCountMid);
            $l2 = array_reverse(array_slice($map, $teamCountMid));
            $round = [];

            for ($j = 0; $j < $teamCountMid; $j++) {
                $t1 = $teams[$l1[$j]];
                $t2 = $teams[$l2[$j]];
                if ($j == 0 && $i % 2 != 0) {
                    array_push($round, [$t2, $t1]);
                } else {
                    array_push($round, [$t1, $t2]);
                }
            }
            array_push($weekMatches, $round);

            $previousLastElement = array_slice($map, $teamCountMid, -1);
            $beforeToMiddleElement = array_slice($map, 0, $teamCountMid);
            $lastElement = array_slice($map, -1);

            $map = array_merge($previousLastElement, $beforeToMiddleElement, $lastElement);
        }

        return $weekMatches;
    }

    /**
     * Save match.
     *
     * @param array $weekMatches
     * @param int $teamCount
     * @return void
     */
    public function saveMatch(array $weekMatches, int $teamCount): void
    {
        $count = 1;
        foreach ($weekMatches as $weekMatch) {
            foreach ($weekMatch as $match) {
                if ($match[0]['id'] && $match[1]['id']) {
                    $this->matchRepository->createMatch([
                        "week" => $count,
                        "team_one_id" => $match[0]['id'],
                        "team_two_id" => $match[1]['id']
                    ]);

                    $this->matchRepository->createMatch([
                        "week" => (2 * ($teamCount - 1)) + 1 - $count,
                        "team_one_id" => $match[1]['id'],
                        "team_two_id" => $match[0]['id']
                    ]);
                }
            }
            $count++;
        }
    }

    /**
     * All matches.
     *
     * @return Collection
     */
    public function getAllMatches(): Collection
    {
        return collect(MatchResource::collection($this->matchRepository->all()))->groupBy('week');
    }

    /**
     * Schedule matches for play all
     *
     * @return array
     * @throws AnyMatchNotFoundException
     */
    public function scheduleForPlayAll(): array
    {
        $checkUnPlayedMatch = $this->matchRepository->checkUnPlayedMatch();

        if (!$checkUnPlayedMatch) {
            throw new AnyMatchNotFoundException('All matches are played');
        }

        for ($i = 1; $i <= $this->weekCount; $i++) {
            if (count($this->matchRepository->pendingMatches($i))) {
                $matchResults[] = MatchResource::collection($this->saveMatchResults($this->matchRepository->pendingMatches($i)));
            } else {
                $matchResults[] = MatchResource::collection($this->matchRepository->weeklyMatches($i));
            }

            $matchPredictions[] = PredictionResource::collection($this->matchPredictions($this->teamStatisticService->teamStatistics($i), $i));

            $teamStatistics[] = $this->teamStatisticService->teamStatistics($i);
        }

        return [
            'matchResults' => $matchResults ?? [],
            'teamStatistics' => $teamStatistics ?? [],
            'matchPredictions' => $matchPredictions ?? [],
        ];
    }

    /**
     * Schedule matches
     *
     * @param int $week
     * @return array|Exception|int
     * @throws ThisWeekMatchNotFoundException
     * @throws WeekMatchNotFoundException
     */
    public function schedule(int $week): array|Exception|int
    {
        if (!$this->matchRepository->notPlayedWeeks() || $this->matchRepository->notPlayedWeeks() < $week) {
            throw new ThisWeekMatchNotFoundException('There is currently no game this week');
        }

        if ($this->weekCount < $week) {
            throw new WeekMatchNotFoundException('The week you mentioned is not on the schedule');
        }

        if (count($this->matchRepository->pendingMatches($week))) {
            $matchResults = MatchResource::collection($this->saveMatchResults($this->matchRepository->pendingMatches($week)));
        } else {
            $matchResults = MatchResource::collection($this->matchRepository->weeklyMatches($week));
        }

        return [
            'matchResults' => $matchResults,
            'teamStatistics' => $this->teamStatisticService->teamStatistics($week),
            'matchPredictions' => PredictionResource::collection($this->matchPredictions($this->teamStatisticService->teamStatistics($week), $week)),
            'lastWeek' => $this->weekCount,
        ];
    }

    /**
     * Save match results.
     *
     * @param Collection $pendingMatches
     * @return Collection
     */
    public
    function saveMatchResults(Collection $pendingMatches): Collection
    {
        if (count($pendingMatches)) {
            foreach ($pendingMatches as $pendingMatch) {
                $team_one_random_goals = rand(0, 5);
                $team_two_random_goals = rand(0, 5);

                $this->matchRepository->saveMatchResults($pendingMatch, [
                    'team_one_goals' => $team_one_random_goals,
                    'team_two_goals' => $team_two_random_goals,
                    'win' => getMatchScore($team_one_random_goals, $team_two_random_goals),
                ]);

                $this->createTeamStatistic($pendingMatch->refresh(), $pendingMatch->teamOne->id, 1);
                $this->createTeamStatistic($pendingMatch->refresh(), $pendingMatch->teamTwo->id, 2);
            }
        }

        return $pendingMatches;
    }

    /**
     * Create team statistics.
     *
     * @param Matches $pendingMatch
     * @param int $teamId
     * @param int $position
     * @return void
     */
    public
    function createTeamStatistic(Matches $pendingMatch, int $teamId, int $position): void
    {
        $lastMatchHistory = $this->teamStatisticService->lastMatchStatistic($teamId);

        $this->teamStatisticService->saveStatistics([
            'week' => $pendingMatch->week,
            'team_id' => $teamId,
            'points' => getTeamPoints($lastMatchHistory->points ?? 0, $pendingMatch->win, $position),
            'played' => $lastMatchHistory ? ++$lastMatchHistory->played : 1,
            'won' => getTeamStatistic($lastMatchHistory, $pendingMatch->win, $position, 'won'),
            'drawn' => getTeamStatistic($lastMatchHistory, $pendingMatch->win, 3, 'drawn'),
            'lost' => getTeamStatistic($lastMatchHistory, $pendingMatch->win, $position == 1 ? 2 : 1, 'lost'),
            'gd' => getTeamGoalDifference($lastMatchHistory, $pendingMatch, $position),
        ]);
    }

    /**
     * Match predictions.
     *
     * @param Collection $teamStatistics
     * @param int $week
     * @return Collection|array
     */
    public
    function matchPredictions(Collection $teamStatistics, int $week): Collection|array
    {
        $potentialPoints = ($this->weekCount - $week) * 3;

        if ($week < $this->weekCount * 0.6 && $this->teamStatisticService->checkAnyZeroPoint($week)) {

            $firstStageTeamPoints = $this->teamStatisticService->firstStagePoint($week) + 1;

            $possibleChampionsPointsSum = $this->teamStatisticService->possibleChampionsPointsSum($week, $firstStageTeamPoints, $potentialPoints) + $this->teamCount;

            foreach ($teamStatistics as $teamStatistic) {
                $teamStatistic->team_percent = (int)round((((($teamStatistic->points + 1) / $possibleChampionsPointsSum)) * 100));
                $teamStatistic->save();
            }

        } else {
            $firstStageTeamPoints = $this->teamStatisticService->firstStagePoint($week);

            $possibleChampionsPointsSum = $this->teamStatisticService->possibleChampionsPointsSum($week, $firstStageTeamPoints, $potentialPoints);

            foreach ($teamStatistics as $teamStatistic) {
                if ($teamStatistic->points >= $firstStageTeamPoints - $potentialPoints) {
                    $teamStatistic->team_percent = (int)round(((($teamStatistic->points / $possibleChampionsPointsSum)) * 100));
                } else {
                    $teamStatistic->team_percent = 0;
                }
                $teamStatistic->save();
            }
        }

        return $teamStatistics;
    }
}
