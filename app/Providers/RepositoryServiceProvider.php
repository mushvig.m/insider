<?php

namespace App\Providers;

use App\Repositories\Interfaces\IMatchRepository;
use App\Repositories\Interfaces\ITeamRepository;
use App\Repositories\Interfaces\ITeamStatisticRepository;
use App\Repositories\MatchRepository;
use App\Repositories\TeamRepository;
use App\Repositories\TeamStatisticRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ITeamRepository::class, TeamRepository::class);
        $this->app->bind(IMatchRepository::class, MatchRepository::class);
        $this->app->bind(ITeamStatisticRepository::class, TeamStatisticRepository::class);
    }
}
