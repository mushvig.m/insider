<?php

namespace App\Exceptions;

use App\Traits\JsonResponseTrait;
use Exception;
use Illuminate\Http\Response;

class WeekMatchNotFoundException extends Exception
{
    use JsonResponseTrait;


    /**
     * Render the exception into an HTTP response.
     *
     * @return Response
     */
    public function render($exception)
    {
        if ($exception instanceof WeekMatchNotFoundException) {
            return $this->failure($exception->getMessage());
        }
    }
}
