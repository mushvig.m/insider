<?php

namespace App\Repositories;

use App\Models\Matches;
use App\Repositories\Interfaces\IMatchRepository;
use Illuminate\Support\Collection;

class MatchRepository extends BaseRepository implements IMatchRepository
{
    /**
     * MatchRepository constructor.
     *
     * @param Matches $model
     */
    public function __construct(Matches $model)
    {
        parent::__construct($model);
    }

    /**
     * Delete all matches.
     *
     * @return void
     */
    public function deleteAllMatch(): void
    {
        $this->model->whereNotNull('id')->delete();
    }

    /**
     * Create team.
     *
     * @param array $attributes
     * @return void
     */
    public function createMatch(array $attributes): void
    {
        $this->model->create($attributes);
    }

    /**
     * All matches.
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * Number of weeks not played
     *
     * @return int|null
     */
    public function notPlayedWeeks(): int|null
    {
        return $this->model->orderBy('week', 'ASC')->where('win', false)->value('week');
    }

    /**
     * Pending matches.
     *
     * @param int $week
     * @return Collection
     */
    public function pendingMatches(int $week): Collection
    {
        return $this->model->where('win', false)->where('week', $week)->get();
    }

    /**
     * Save match results.
     *
     * @param Matches $pendingMatch
     * @param array $attributes
     * @return void
     */
    public function saveMatchResults(Matches $pendingMatch, array $attributes): void
    {
        $this->model->where('id', $pendingMatch->id)->update($attributes);
    }

    /**
     * Weekly matches.
     *
     * @param int $week
     * @return Collection
     */
    public function weeklyMatches(int $week): Collection
    {
        return $this->model->where('week', $week)->get();
    }

    /**
     * Check any unPlayed match.
     *
     * @return bool
     */
    public function checkUnPlayedMatch(): bool
    {
        return $this->model->where('win', false)->exists();
    }

}
