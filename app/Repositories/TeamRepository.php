<?php

namespace App\Repositories;

use App\Models\Team;
use App\Repositories\Interfaces\ITeamRepository;

class TeamRepository extends BaseRepository implements ITeamRepository
{

    /**
     * The id implementation.
     *
     * @var int
     */
    private int $id;

    /**
     * The week implementation.
     *
     * @var int
     */
    private int $week;

    /**
     * TeamRepository constructor.
     *
     * @param Team $model
     */
    public function __construct(Team $model)
    {
        parent::__construct($model);
    }

    /**
     * Set id.
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set week.
     *
     * @param int $week
     * @return void
     */
    public function setWeek(int $week): void
    {
        $this->week = $week;
    }

    /**
     * Get week.
     *
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * Update active Teams.
     *
     * @return void
     */
    public function updateAllActive(): void
    {
        $this->model->whereActive(true)->update([
            'active' => false
        ]);
    }

    /**
     * Get random Team.
     *
     * @return int
     */
    public function getRandomTeam(): int
    {
        return $this->model->whereActive(false)->inRandomOrder()->value('id');
    }

    /**
     * Create team.
     *
     * @return void
     */
    public function createTeam(): void
    {
        $this->model->find($this->getId())->update(['active' => true]);
    }

    /**
     * Get all active team.
     *
     * @return array
     */
    public function getAllActive(): array
    {
        return $this->model->whereActive(true)->get(['id', 'name'])->toArray();
    }

    /**
     * Get team count.
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->model->whereActive(true)->count();
    }

}
