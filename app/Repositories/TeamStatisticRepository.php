<?php

namespace App\Repositories;

use App\Models\TeamStatistic;
use App\Repositories\Interfaces\ITeamStatisticRepository;
use Illuminate\Support\Collection;

class TeamStatisticRepository extends BaseRepository implements ITeamStatisticRepository
{
    /**
     * The id implementation.
     *
     * @var int
     */
    private int $id;

    /**
     * The week implementation.
     *
     * @var int
     */
    private int $week;

    /**
     * TeamStatisticRepository constructor.
     *
     * @param TeamStatistic $model
     */
    public function __construct(TeamStatistic $model)
    {
        parent::__construct($model);
    }


    /**
     * Save statistics.
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set week.
     *
     * @param int $week
     * @return void
     */
    public function setWeek(int $week): void
    {
        $this->week = $week;
    }

    /**
     * Get week.
     *
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * Delete statistics.
     *
     * @return void
     */
    public function deleteAllStatistics(): void
    {
        $this->model->whereNotNull('id')->delete();
    }

    /**
     * Last match statistic.
     *
     * @param int $teamId
     * @return TeamStatistic|null
     */
    public function lastMatchStatistic(int $teamId): TeamStatistic|null
    {
        return $this->model->where('team_id', $teamId)->get()->last();
    }

    /**
     * Save statistics.
     *
     * @param array $attributes
     * @return void
     */
    public function saveStatistics(array $attributes): void
    {
        $this->model->create($attributes);
    }

    /**
     * Team statistics.
     *
     * @return Collection
     */
    public function teamStatistics(): Collection
    {
        return $this->model->where('week', $this->getWeek())->orderByDesc('points')->orderByDesc('gd')->get();
    }

    /**
     * First stage point.
     *
     * @return int
     */
    public function firstStagePoint(): int
    {
        return $this->model->where('week', $this->getWeek())->orderByDesc('points')->value('points');
    }

    /**
     * Possible champion points sum.
     *
     * @param int $firstStageTeamPoints
     * @param int $potentialPoints
     * @return int
     */
    public function possibleChampionsPointsSum(int $firstStageTeamPoints, int $potentialPoints): int
    {
        return $this->model->where([['week', $this->getWeek()], ['points', '>=', $firstStageTeamPoints - $potentialPoints]])->get()->sum('points');
    }

    /**
     * Check any zero point.
     *
     * @return bool
     */
    public function checkAnyZeroPoint(): bool
    {
        return $this->model->where(['week' => $this->getWeek(), 'points' => false])->exists();
    }

}
