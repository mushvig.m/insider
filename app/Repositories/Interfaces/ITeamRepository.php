<?php

namespace App\Repositories\Interfaces;

use App\Models\TeamStatistic;
use Illuminate\Support\Collection;

interface ITeamRepository
{
    /**
     * Update active teams.
     *
     * @return void
     */
    public function updateAllActive(): void;

    /**
     * Get random Team.
     *
     * @return int
     */
    public function getRandomTeam(): int;

    /**
     * Create team.
     *
     * @return void
     */
    public function createTeam(): void;

    /**
     * Get all active team.
     *
     * @return array
     */
    public function getAllActive(): array;

    /**
     * Get team count.
     *
     * @return int
     */
    public function getCount(): int;

}
