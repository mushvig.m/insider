<?php

namespace App\Repositories\Interfaces;

use App\Models\TeamStatistic;
use Illuminate\Support\Collection;

interface ITeamStatisticRepository
{
    /**
     * Delete statistics.
     *
     * @return void
     */
    public function deleteAllStatistics(): void;

    /**
     * Last match statistic.
     *
     * @param int $teamId
     * @return TeamStatistic|null
     */
    public function lastMatchStatistic(int $teamId): TeamStatistic|null;

    /**
     * Save statistics.
     *
     * @param array $attributes
     * @return void
     */
    public function saveStatistics(array $attributes): void;

    /**
     * Team statistics.
     *
     * @return Collection
     */
    public function teamStatistics(): Collection;

    /**
     * First stage point.
     *
     * @return int
     */
    public function firstStagePoint(): int;

    /**
     * Possible champion points sum.
     *
     * @param int $firstStageTeamPoints
     * @param int $potentialPoints
     * @return int
     */
    public function possibleChampionsPointsSum(int $firstStageTeamPoints, int $potentialPoints): int;

    /**
     * Check any zero point.
     *
     * @return bool
     */
    public function checkAnyZeroPoint(): bool;

}
