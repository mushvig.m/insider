<?php

namespace App\Repositories\Interfaces;

use App\Models\Matches;
use Illuminate\Support\Collection;

interface IMatchRepository
{
    /**
     * Delete all matches.
     *
     * @return void
     */
    public function deleteAllMatch(): void;

    /**
     * Create match.
     *
     * @param array $attributes
     * @return void
     */
    public function createMatch(array $attributes): void;

    /**
     * All matches.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * Number of weeks not played
     *
     * @return int|null
     */
    public function notPlayedWeeks(): int|null;

    /**
     * Pending matches.
     *
     * @param int $week
     * @return Collection
     */
    public function pendingMatches(int $week): Collection;

    /**
     * Save match results.
     *
     * @param Matches $pendingMatch
     * @param array $attributes
     * @return void
     */
    public function saveMatchResults(Matches $pendingMatch, array $attributes): void;

    /**
     * Weekly matches.
     *
     * @param int $week
     * @return Collection
     */
    public function weeklyMatches(int $week): Collection;

    /**
     * Check any unPlayed match.
     *
     * @return bool
     */
    public function checkUnPlayedMatch(): bool;

}
