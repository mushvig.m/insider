<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AnyMatchNotFoundException;
use App\Exceptions\ThisWeekMatchNotFoundException;
use App\Exceptions\WeekMatchNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\WeeklyResultsRequest;
use App\Services\MatchService;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Response;

class ResultsController extends Controller
{
    use JsonResponseTrait;

    /**
     * The match service implementation.
     *
     * @var MatchService
     */
    protected MatchService $matchService;

    /**
     * Create a new match service instance.
     *
     * @param MatchService $matchService
     * @return void
     */
    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    /**
     * Create match results for play all.
     *
     * @return Response
     * @throws AnyMatchNotFoundException
     */
    public function resultsForPlayAll(): Response
    {
        $results = $this->matchService->scheduleForPlayAll();

        return $this->success('success', compact('results'));
    }

    /**
     * Create match results.
     *
     * @param WeeklyResultsRequest $request
     * @return Response
     * @throws ThisWeekMatchNotFoundException
     * @throws WeekMatchNotFoundException
     */
    public function results(WeeklyResultsRequest $request): Response
    {
        $results = $this->matchService->schedule($request->validated()['week']);

        return $this->success('success', compact('results'));
    }

}
