<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTournamentScheduleRequest;
use App\Services\MatchService;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Response;

class TournamentScheduleController extends Controller
{
    use JsonResponseTrait;

    /**
     * The match service implementation.
     *
     * @var MatchService
     */
    protected MatchService $matchService;

    /**
     * Create a new match service instance.
     *
     * @param MatchService $matchService
     * @return void
     */
    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    /**
     * Create a new tournament schedule.
     *
     * @param CreateTournamentScheduleRequest $request
     * @return Response
     */
    public function createTournamentSchedule(CreateTournamentScheduleRequest $request): Response
    {
        $weekMatches = $this->matchService->createMatch($request);

        return $this->success('success', $weekMatches);
    }
}
