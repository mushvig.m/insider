<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Homepage.
     *
     * @var View
     */
    public function index(): View
    {
        return view('index');
    }

    /**
     * Weekly matches.
     *
     * @var View
     */
    public function weeklyResults(): View
    {
        return view('weekly-results');
    }
}
