<?php

use Illuminate\Database\Eloquent\Model;

/**
 * Relation table value
 *
 * @return Model|string
 */
if (!function_exists('getRelationValue')) {
    function getRelationValue($result, $relationTable, $value): Model|string
    {
        return $result && $result->$relationTable ? $result->$relationTable->$value : '';
    }
}

/**
 * Team points
 *
 * @return int
 */
if (!function_exists('getTeamPoints')) {
    function getTeamPoints($points, $win, $position): int
    {
        return $win == 3 ? ++$points : ($win == $position ? 3 + $points : $points);
    }
}

/**
 * Match score
 *
 * @return int
 */
if (!function_exists('getMatchScore')) {
    function getMatchScore($team_one_random_goals, $team_two_random_goals): int
    {
        return $team_one_random_goals > $team_two_random_goals ? 1 : ($team_one_random_goals < $team_two_random_goals ? 2 : 3);
    }
}

/**
 * Team statistics
 *
 * @return int
 */
if (!function_exists('getTeamStatistic')) {
    function getTeamStatistic($lastMatchHistory, $win, $position, $approximate): int
    {
        if ($lastMatchHistory) {
            $result = $win == $position ? ++$lastMatchHistory->$approximate : $lastMatchHistory->$approximate;
        } else {
            $result = $win == $position ? 1 : 0;
        }

        return $result;
    }
}

/**
 * Team GD
 *
 * @return int
 */
if (!function_exists('getTeamGoalDifference')) {
    function getTeamGoalDifference($lastMatchHistory, $match, $position): int
    {
        if ($lastMatchHistory) {
            $result = $position == 1 ? $lastMatchHistory->gd + ($match->team_one_goals - $match->team_two_goals) : $lastMatchHistory->gd + ($match->team_two_goals - $match->team_one_goals);
        } else {
            $result = $position == 1 ? $match->team_one_goals - $match->team_two_goals : $match->team_two_goals - $match->team_one_goals;
        }

        return $result;
    }
}

/**
 * Week count.
 *
 * @return int
 */
if (!function_exists('getWeekCount')) {
    function getWeekCount($teamCount): int
    {
        return $teamCount % 2 == 0 ? ($teamCount - 1) * 2 : $teamCount * 2;
    }
}

?>
