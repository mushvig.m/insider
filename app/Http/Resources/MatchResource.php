<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_one' => getRelationValue($this, 'teamOne', 'name'),
            'team_two' => getRelationValue($this, 'teamTwo', 'name'),
            'team_one_goals' => $this->team_one_goals,
            'team_two_goals' => $this->team_two_goals,
            'week' => $this->week,
        ];
    }
}
