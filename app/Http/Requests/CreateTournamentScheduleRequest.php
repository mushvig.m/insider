<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateTournamentScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'teamCount' => ['required_if:load,false', 'integer', 'min:4', 'max:20'],
        ];
    }

    public function messages()
    {
        return [
            'teamCount.required' => 'This field must be filled',
            'teamCount.min' => 'The count of teams should be between 4 and 20',
            'teamCount.max' => 'The count of teams should be between 4 and 20',
            'teamCount.integer' => 'This field must be of integer type',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => $validator->errors()
        ], 422));
    }
}
