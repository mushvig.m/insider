<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public $table = "teams";

    public function statistics()
    {
        return $this->hasMany(TeamStatistic::class);
    }

    public function lastMatchStatistic()
    {
        return $this->hasOne(TeamStatistic::class)->orderByDesc('week');
    }
}
