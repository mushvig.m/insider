<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamStatistic extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public $table = "team_statistics";

    protected $with = 'team';

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

}
