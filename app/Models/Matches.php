<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Matches extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public $table = "matches";

    protected $with = ['teamOne', 'teamTwo'];

    /**
     * Get first team.
     */
    public function teamOne()
    {
        return $this->belongsTo(Team::class, 'team_one_id', 'id');
    }

    /**
     * Get second team.
     */
    public function teamTwo()
    {
        return $this->belongsTo(Team::class, 'team_two_id', 'id');
    }


}
