@extends('main.layouts')
@section('content')

    <div
        class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        <a href="/" class="btn btn-danger back">Back to homepage</a>
        <section class="content-info">
            <div class="container-fluid paddings-mini">

                <div class="row results">

                </div>
                <div class="row buttons">
                    <div class="col-md-6">
                        <div class="button-item">
                            <button id="play-all" class="btn btn-success">Play all</button>
                            <button id="next-week" data-week="1" class="btn btn-primary">Next week</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset("assets/js/results.js") }}"></script>

@endsection
