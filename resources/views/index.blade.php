@extends('main.layouts')
@section('content')

    <div
        class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

        <div class="container-fluid paddings-mini section-block">
            <div class="row">
                <div class="col-lg-12 mt-3 d-flex justify-content-center flex-column align-items-center">
                    <p>To track statistics and tournament schedule, please create a tournament schedule first</p>
                    <input type="number" class="form-control team-count mb-3" value=""
                           name="teamCount"
                           placeholder="Team count"/>
                    <button id="create-tournament-schedule" class="btn btn-success">Create Tournament Schedule</button>
                </div>
            </div>
            <div class="row matches"></div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset("assets/js/schedule.js") }}"></script>

@endsection
