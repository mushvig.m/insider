<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Insider football tournament</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('assets/images/favicon.ico') }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assets/css/boostrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>
<body class="antialiased">

@yield('content')


<script src="{{ asset("assets/js/jquery-3.6.0.min.js") }}"></script>
<script src="{{ asset("assets/js/general.js") }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@yield("scripts")

</body>
</html>
